use crate::database::search::pagination::Pagination;
use crate::database::search::fields::filter_field::FilterField;
use crate::database::search::fields::sort_field::SortField;
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct SearchRequest {
    #[serde(rename="filterFields")]
    pub filter_fields: Vec<FilterField>,
    #[serde(rename="sortFields")]
    pub sort_fields: Vec<SortField>,
    pub pagination: Option<Pagination>
}
use crate::database::search::search_request::SearchRequest;
use crate::RpaError;

pub fn validate_request(request: SearchRequest) -> Result<(), RpaError> {
    if request.pagination.is_some() {
        let pagination = request.pagination.unwrap();
        if pagination.page <= 0 {
            return Err(RpaError::builder().with_description("Page has to be greater than 0").build());
        }
        if pagination.page_size <= 0 {
            return Err(RpaError::builder().with_description("PageSize has to be greater than 0").build());
        }
    }
    Ok(())
}
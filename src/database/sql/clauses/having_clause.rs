use crate::database::sql::enums::operator::Operator;
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct HavingClause {
    pub condition: String,
    pub operator: Operator,
    pub value: String
}
use crate::database::sql::enums::join_type::JoinType;
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct JoinClause {
    pub join_table: String,
    pub from_field: String,
    pub to_field: String,
    pub join_type: JoinType
}
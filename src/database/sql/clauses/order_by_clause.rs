use serde::{Serialize, Deserialize};
use crate::database::sql::enums::order::Order;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct OrderByClause {
    pub condition: String,
    pub order: Order
}
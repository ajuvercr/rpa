use diesel::sql_types::BigInt;

#[derive(QueryableByName)]
pub struct CountQuery {
    #[sql_type = "BigInt"]
    pub count_result: i64
}

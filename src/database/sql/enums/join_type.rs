use serde::{Serialize, Deserialize};
use crate::database::sql::enums::sql_symbol::SqlSymbol;

#[allow(non_camel_case_types)]
#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum JoinType {
    LEFT, INNER, LEFT_OUTER, RIGHT, RIGHT_OUTER, CROSS, STRAIGHT_JOIN
}

impl SqlSymbol for JoinType {
    fn symbol(self) -> &'static str {
        return match self {
            JoinType::LEFT => "INNER JOIN",
            JoinType::LEFT_OUTER => "INNER JOIN",
            JoinType::INNER => "INNER JOIN",
            JoinType::RIGHT => "INNER JOIN",
            JoinType::RIGHT_OUTER => "RIGHT OUTER JOIN",
            JoinType::CROSS => "CROSS JOIN",
            JoinType::STRAIGHT_JOIN => "STRAIGHT_JOIN"
        }
    }
}
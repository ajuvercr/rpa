use serde::{Serialize, Deserialize};
use crate::database::sql::enums::sql_symbol::SqlSymbol;

#[allow(non_camel_case_types)]
#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum Operator {
    LESS, EQUALS, GREATER, DISTINCT, LESS_OR_EQUALS, GREATER_OR_EQUALS, LIKE
}

impl SqlSymbol for Operator {
    fn symbol(self) -> &'static str {
        return match self {
            Operator::LESS => "<",
            Operator::EQUALS => "=",
            Operator::GREATER => ">",
            Operator::DISTINCT => "<>",
            Operator::LESS_OR_EQUALS => "<=",
            Operator::GREATER_OR_EQUALS => ">=",
            Operator::LIKE => "LIKE"
        }
    }
}
use serde::{Serialize, Deserialize};
use crate::database::sql::enums::sql_symbol::SqlSymbol;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum Order {
    ASC, DESC
}

impl SqlSymbol for Order {
    fn symbol(self) -> &'static str {
        return match self {
            Order::ASC => "ASC",
            Order::DESC => "DESC"
        }
    }
}
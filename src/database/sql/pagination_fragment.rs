#[derive(Clone, Debug)]
pub struct PaginationFragment {
    pub query: String,
    pub page: i64,
    pub page_size: i64,
    pub total_pages: i64
}